.. index::
   ! Glossaire

.. _glossaire:

===================================================================================
Glossaire
===================================================================================

.. glossary::


   CAA
   Campaign Against Antisemitism

       Campaign Against Antisemitism is a volunteer-led charity dedicated
       to exposing and countering #antisemitism through education and
       #zerotolerance law enforcement

       - https://antisemitism.org/
       - https://twitter.com/antisemitism


   EHRC
   Equality and Human Rights Commission

       Promoting and upholding equality and human rights ideals and laws
       across England, Scotland and Wales.

       The Equality and Human Rights Commission is Great Britain’s national
       equality body and has been awarded an ‘A’ status as a National
       Human Rights Institution (NHRI) by the United Nations.

       Our job is to help make Britain fairer. We do this by safeguarding
       and enforcing the laws that protect people’s rights to fairness,
       dignity and respect.

       - https://www.equalityhumanrights.com/en
       - https://www.equalityhumanrights.com/en/about-us/who-we-are
       - https://twitter.com/EHRC
       - https://www.equalityhumanrights.com/sites/default/files/investigation-into-antisemitism-in-the-labour-party.pdf


   GT
       Groupe de Travail


   Jewish Labour Movement

       The Jewish Labour Movement, précédemment connu sous le nom de Poale Zion
       (Britain) de 1903 à 2004, est l’une des plus anciennes organisations
       socialistes (« socialist societies ») affiliées au parti travailliste britannique.

       The Jewish Labour Movement is a membership organisation of Labour
       supporting members of the Jewish Community and a formal affiliate
       of the Labour Party in the Uk since 1920

       - https://www.jewishlabour.uk/
       - https://twitter.com/JewishLabour



   Jewish Voice for Labour
       The Jewish Voice for Labour est une organisation créée en 2017 au sein
       du Labour pour ses membres de confession juive, visant à lutter
       contre toute forme de racisme et d’antisémitisme.

       Priorities: universal human rights & dignity;justice for all;
       freedom of expression; democracy in the Labour Party

       - https://www.jewishvoiceforlabour.org.uk/
       - https://twitter.com/jvoicelabour

       - https://www.jewishvoiceforlabour.org.uk/article/institutional-racism-the-case-of-the-labour-party/

   LAAS
   Labour Against Antisemitism

       Labour Against Antisemitism was started by Labour members to combat
       what we recognised as the rise of anti-Jewish sentiment in the
       Labour Party.

       We are a voluntary organisation and we hold fighting antisemitism
       as our main aim above all else and any other allegiances.

       Our aim is to eradicate antisemitism from the Party completely and
       become a redundant organisation. However, it appears that this is
       an unlikely scenario in the near future, so we expose and report
       Labour antisemitism in the hope it will become socially unacceptable.

       Labour antisemitism can appear as Soviet era style racism, far
       right style racism, Medieval racism or religious racism.
       Whichever guise it appears in, the effect is the same for the
       Jewish community - racist abuse.



       - https://labouragainstas.org.uk/
       - https://twitter.com/LabourAgainstAS
