.. index::
   pair: Motion ; EELV

.. _motion_gt_antisemitisme:

===================================================================================
Motion création du groupe de travail : **Lutte contre l’antisémitisme** EELV
===================================================================================

- https://www.eelv.fr/files/2021/10/MB-GT-antisemitisme-CF2021100203.pdf
- https://www.eelv.fr/mb-gt-ri-antisemitisme-cf2021100203/
- :ref:`gt_antisemitisme`

Création du groupe de travail : “Lutte contre l’antisémitisme”
==================================================================

Adoptée par le Conseil fédéral d’Europe Écologie Les Verts des 2 et 3 octobre 2021

Exposé des motifs
-------------------

Lors de l'été 2021, à l’occasion du mouvement anti pass sanitaire et anti-vaccin,
qui a réuni entre 150 000 et 250 000 personnes dans la rue chaque samedi,
la persistance et la virulence du discours antisémite ont pu être à
nouveau constatées.

Certes, la majorité des manifestant·e·s ne se reconnaissaient pas dans
ce discours et ces pancartes, plus ou moins explicites, qui faisaient
référence à l’imaginaire antisémite [1].

**Mais l’antisémitisme était bien présent**, et pas seulement de manière
anecdotique, car loin d’être seulement cantonné à la marge des cortèges,
il semblait irriguer une partie importante des analyses complotistes
de la situation sanitaire et de la critique du pouvoir [2].

De plus, et sans doute pour la première fois, des associations et des
partis qui se revendiquent pourtant progressistes et humanistes, ont
accepté de défiler aux côtés de différents mouvements d’extrême-droite
et populistes, qui ont remis au goût du jour et sans vergogne les vieilles
rengaines antisémites, signe inquiétant d’une certaine banalisation
de ce type de discours.

**Sur les réseaux sociaux, et malgré la loi française, l’antisémitisme 2.0
se développe aussi sans retenue**. Récemment, un site dressait une liste
des patronymes juifs dans les médias, parmi la classe politique et les
médecins [3], pendant que les dirigeants d’un célèbre site français
de financement participatif assumaient tranquillement d’héberger des
contenus ouvertement antisémites et complotistes [4] ; divers groupuscules
d’extrême-droite se sont d’ailleurs constitués en ligne ces dernières années,
dont les meneurs accèdent parfois au statut « d’influenceurs » et répandent
une esthétique ouvertement violente et raciste [5].

Cette libération de la parole antisémite a des conséquences graves.
En 2019, selon le Ministère de l’Intérieur, « 687 faits à caractère
antisémite ont été constatés », dont 151 atteintes à des personnes
ou des biens.
C’est 140 faits supplémentaires que l’année précédente [6].

Il est encore courant d’observer des tags antisémites devant les logements
de personnes juives ou dans les cimetières confessionnels, et depuis 2006
au moins dix français·e·s sont mort·e·s parce que juifs.

L’antisémitisme a ses spécificités, par son ampleur, sa persistance dans
l’histoire et sa diversité d’expression. Il touche aussi bien des mouvements
de gauche que de droite, la littérature ou les médias, et les exemples
en sont trop nombreux. Depuis Maurras jusqu’à Proudhon, des pamphlets
de Céline aux spectacles de Dieudonné, des dessins « humoristiques »
du jeune Yann Moix [7] aux délires des QAnons francophones… la haine des
juifs est semble-t-il un cliché bien partagé en France, que l’on retrouve
aussi dans des déclarations de nombreuses personnalités politiques
françaises à travers l’histoire, et pas seulement là où les attend
le plus, c’est-à-dire à l’extrême-droite.

Le vieil antisémitisme chrétien, celui du « peuple déicide », reste
toujours d’actualité, on a pu le retrouver dans des publications en ligne
d’une célèbre députée de confession catholique quand elle s’inquiète de
la loi sur le mariage homosexuel [8] ; aujourd’hui il se raccroche
volontiers aux wagons du moment : un groupuscule catholique intégriste,
qui appelle par ailleurs à « la séparation du CRIF et de l’État » [9]
et conseille la lecture de Maurras à ses adhérents, était présent aux
manifestations de cet été contre la vaccination et le pass sanitaire.

Par ailleurs, à propos du conflit israélo-palestinien, la critique de la
politique des gouvernements israéliens successifs, depuis l’assassinat
d’Yitzhak Rabin en 1995 qui marque la fin du processus de paix des
Accords d’Oslo [10], est régulièrement instrumentalisé par le discours
antisémite, souvent sous le prétexte de la lutte anti-impérialiste et
tiers-mondiste.
Cet antisémitisme est condamnable en tant que tel, mais aussi parce qu’il
affaiblit le combat pour une paix juste et durable dans la région entre
les Israéliens et les Palestiniens, ainsi que l’application de la
résolution 2334 de l’ONU [11].

En se limitant au 21e siècle, de nombreux responsables politiques venant
de la droite, du centre mais aussi des différentes tendances de la gauche,
ont en effet créé la polémique et soulevé l’indignation d’une grande
partie de la communauté juive, à la suite de propos ou d’iconographies
proprement antisémites ou jugés comme tels.

Les multiples incarnations de l’antisémitisme se traduisent ainsi en
instrumentalisation de conflits internationaux, en haine répandue sur
les réseaux sociaux, elles alimentent la rhétorique complotiste, sont
au cœur d’une extrême-droite qui s’organise et se banalise, elles dévoient
la nécessaire et juste critique des méfaits de la mondialisation et des
ravages sociaux du néolibéralisme financier.

Au-delà d’engagements individuels, locaux ou épisodiques, **il nous paraît
donc nécessaire qu’EELV prenne clairement position contre l’antisémitisme
latent qui traverse l’entièreté de la société française**.

Au-delà d’un engagement moral contre l’antisémitisme, EELV doit être en
mesure de développer un corpus programmatique à la hauteur des enjeux actuels,
d’autant plus que **l’écologie politique se veut à l’avant-garde des combats
contre toutes les discriminations**.

**Constitué à l’heure actuelle d’une vingtaine de militant·e·s réuni·e·s
via un groupe Signal**, le groupe de travail « Lutte contre l’antisémitisme »,
dont nous proposons donc l’officialisation par le Conseil Fédéral, a
entamé depuis mai 2021 un effort de documentation et de veille, a déjà
tenu plusieurs réunions de travail en visioconférence et a rencontré
le GT « Lutte contre le Front National » afin d’échanger sur les synergies
communes.

En bonne intelligence avec les commissions thématiques nationales du parti,
notamment Quartiers Populaires et Justice, le GT Lutte contre l’antisémitisme
se propose de travailler notamment sur six axes :

1. Contribuer à un historique des positionnements du parti sur le sujet
   avec constitution d’une bibliographie (en lien avec le site https://www.archivesecolo.org/) ;
2. Esquisser les contours de l’antisémitisme contemporain, notamment
   via ses résurgences en ligne et dans les discours politiques ;
3. Identifier les outils et proposer des solutions pour identifier et
   pour lutter efficacement contre les discours antisémites ;
4. Interroger les programmes de l’écologie politique aux échéances
   électorales (y compris les élections présidentielles et législatives de 2022)
   du point de vue de la lutte contre l’antisémitisme ;
5. Tisser des liens avec les différentes organisations et assurer la
   participation du mouvement aux différentes mobilisations contre l’antisémitisme ;
6. Former et informer les militant·e·s et élu·e·s, notamment celles et
   ceux en responsabilité, sur cet enjeu et sur les politiques publiques
   locales et nationales adéquates pour y répondre.


.. _motion_creation_gt_lutte_antisemitisme:

Motion
=========

En conséquence, le Conseil Fédéral, réuni les 2 et 3 octobre 2021 en
séance plénière, à Paris, décide :

- :ref:`qu’un groupe de travail <gt_antisemitisme>` intitulé
  **Lutte contre l’antisémitisme** est créé au sein du parti EÉLV ;
- que ce :ref:`groupe de travail <gt_antisemitisme>` se donne pour mission:

  - **d’exercer une veille sur les mouvements antisémites actuels et passés**,
    d’en documenter leur histoire et leur actualité, d’interroger aussi
    l’histoire de l’écologie politique et ses programmes sur ces questions,
  - d’émettre des propositions de doctrine et d’action **afin de lutter contre
    l’antisémitisme, partout où il se trouve**,
  - de **proposer des solutions de formation et d’information des élu·e·es et des militant·e·s** ;
- Qu’à cette fin, le Groupe de travail **pourra auditionner différent·e·s
  spécialistes universitaires, mouvements et associations** ;
- Que ce GT travaillera de manière transversale avec tous les organes
  du parti EÉLV, dans un esprit de co-élaboration, notamment avec les
  commissions thématiques nationales ;
- que ce **GT devra rendre compte de sa première année d’activité lors du
  Conseil Fédéral qui se tiendra à la rentrée 2022** ;
- Que le GT puisse proposer des prises de position aux porte-parole d’EÉLV

Notes
=======

- [1] `« Mais qui ? », de la blague virale au slogan antisémite, Le Monde, 10 août 2021 <https://www.lemonde.fr/societe/article/2021/08/10/mais-qui-de-la-blague-virale-au-slogan-antisemite_6091086_3224.html>`_
- [2] `Caricatures, complot, liste de noms : le coronavirus engendre des
  attaques antisémites sur le web, France Inter <https://www.franceinter.fr/caricatures-complot-liste-de-noms-le-coronavirus-engendre-des-attaques-antisemites-sur-le-web>`_, 30 mars 2020
- [3] `#ilssontpartout : le fondateur du site antisémite mis en examen
  pour « injure et provocation à la discrimination”, Le Midi Libre <https://www.midilibre.fr/2021/08/23/ilssontpartout-le-fondateur-du-site-antisemite-mis-en-examen-pour-injure-et-provocation-a-la-discrimination-9746316.php>`_, 23 août 2021
- [4] `Les fondateurs de Tipeee « assument » d’héberger des contenus antisémites
  ou complotistes, Numérama <https://www.numerama.com/politique/736552-les-fondateurs-de-tipeee-assument-dheberger-des-contenus-antisemites-ou-complotistes.html>`_, 3 septembre 2021
- [5] `Ultradroite : derrière le Youtubeur toulousain « Papacito« ,
  d‘autres influenceurs aux gros bras et aux idées radicales, StreetPress <https://www.streetpress.com/sujet/1603295168-vengeance-patriote-groupuscule-extreme-droite-militants-combat-arme-neonazi-fachosphere-politique>`_, 28 octobre 2020
- [6] `Bilan 2019 des actes antireligieux, antisémites, racistes et xénophobes,
  DILCRAH <https://www.gouvernement.fr/bilan-2019-des-actes-antireligieux-antisemites-racistes-et-xenophobes>`_, 28 janvier 2020
- [7] `Moix regrette ses dessins « ratés » et antisémites de jeunesse, Le Parisien <https://www.leparisien.fr/culture-loisirs/tv/moix-regrette-ses-dessins-rates-et-antisemites-de-jeunesse-26-08-2019-8139585.php>`_, 26 août 2019
- [8] `Christine Boutin relaye un article antisémite sur Twitter, Le Parisien <https://www.20minutes.fr/politique/1642751-20150630-christine-boutin-relaye-article-antisemite-twitter>`_, 30 juin 2015
- [9] `Civitas, de l’intégrisme chrétien à l’opposition au passe sanitaire, Le Monde <https://www.lemonde.fr/societe/article/2021/08/18/civitas-de-l-integrisme-chretien-a-l-opposition-au-passe-sanitaire_6091720_3224.html>`_, 18 août 2021
- [10] `Il y a 25 ans, les accords d‘Oslo entre Israéliens et Palestiniens, Le Point (AFP), <https://www.lepoint.fr/monde/il-y-a-25-ans-les-accords-d-oslo-entre-israeliens-et-palestiniens-11-09-2018-2250233_24.php>`_ 11 septembre 2018
- [11] `Israël–Palestine : faire prévaloir le droit international, pour une
  reconnaissance de l’État palestinien, Communiqué de presse EELV, signé par Julien Bayou et Sandra Régol <https://www.eelv.fr/israel-palestine-faire-prevaloir-le-droit-international-pour-une-reconnaissance-de-letat-palestinien/>`_, 26 janvier 2017
- [12] `L’inquiétante arrière-boutique idéologique de Kokopelli, Conspiracy Watch <https://www.conspiracywatch.info/linquietante-arriere-boutique-ideologique-de-kokopelli.html>`_, 27 juin 2019
