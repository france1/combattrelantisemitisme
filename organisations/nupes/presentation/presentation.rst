
.. _nupes_presentation:

======================================================================
**Qui sommes nous**
======================================================================

Face à la progression continue de l’antisémitisme dans notre pays et
partout dans le monde, **nous ne pouvons pas rester les bras croisés**.

Nous avons donc décidé de nous saisir de l’opportunité de la
Nouvelle Union Populaire Écologiste et Sociale et des législatives pour
proposer aux 577 candidates et candidats écologistes, insoumis(es),
socialistes, communistes…, **de s’engager clairement sur cette question**.

Alors que la classe politique, gauche comprise, a trop souvent **déserté
ce combat**, et que, partout, fleurissent les instrumentalisations de
l’antisémitisme, il nous semble **fondamental d’adopter un discours clair
et des pratiques fortes**.

Mémoire, sécurité, école, réseaux sociaux, culture(s), Israël ... notre
:ref:`charte en 10 points <charte_nupes>`  englobe l’ensemble des grandes
questions contemporaines liées à la lutte contre l’antisémitisme.

Construite avec des membres de la société civile et des responsables politiques,
**nous regrettons, malgré de nombreuses relances et tentatives, l’absence
de travail collaboratif avec les autres forces de la NUPES**.

Nous souhaitons que chaque candidate et candidat de la NUPES puisse ainsi
se saisir de cet enjeu en **signant la charte (en totalité ou en partie)**,
afin que la gauche et l'écologie politique fassent entendre une voix
forte et engagée sur ce sujet.

Nous espérons, que toute la diversité de cette alliance historique que
constitue la NUPES pourra se retrouver dans notre charte et dans l’espoir
qu’elle suscitera chez toutes celles et tous ceux pour qui la lutte contre
l’antisémitisme et contre toutes les formes de racisme est une priorité.

Alors que la gauche et l'écologie se veulent à l'avant-garde de la lutte
contre toutes les discriminations, **il est temps de porter des politiques
publiques ambitieuses de lutte contre l’antisémitisme !**
