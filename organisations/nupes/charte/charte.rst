.. index::
   ! Charte contre l'antisémitisme

.. figure:: ../images/antisemitisme_2022.png
   :align: center

.. _charte_nupes:

===================================================================================
️✌️ Charte d'engagement de lutte contre l'antisémitisme des candidat·e·s NUPES ️✌️
===================================================================================

- https://www.charte-lutte-antisemitisme.fr/
- https://www.charte-lutte-antisemitisme.fr/les-signataires
- https://twitter.com/Antisemitisme22
- https://www.facebook.com/Antisemitisme2022
- https://www.eelv.fr/mb-gt-ri-antisemitisme-cf2021100203/
- https://ec.europa.eu/commission/presscorner/detail/fr/ip_21_4990



**Alors que l'ensemble de la classe politique a trop souvent déserté ce combat,
candidat-e de la Nouvelle Union Populaire Écologique et Sociale, je m'engage à**:


1. **Prendre clairement position contre les propos et actes antisémites
   ou négationnistes**, y compris s'ils devaient être le fait de membres
   de mon organisation politique
2. **Permettre aux juives et juifs de France pratiquants d'exercer sans
   crainte leur religion**, en améliorant la sécurité des lieux confessionnels
   (synagogues, écoles, cimetières…) et de leurs abords **et en promouvant la laïcité**
3. Prendre en compte le patrimoine, les histoires et les cultures juives
   dans les politiques culturelles et historiques françaises
4. **Garantir à tous les enfants de culture juive le droit à une scolarité
   sans insulte ou agression antisémite, partout en France**, et donc
   défendre une politique ambitieuse de lutte contre l'antisémitisme et
   contre le complotisme dans les établissements scolaires
5. **Lutter contre les propos et menaces antisémites** qui se répandent sur
   les réseaux sociaux et participer à l'effort législatif nécessaire
   afin de mieux les réprimer
6. **Repenser et développer les politiques de mémoire de la Shoah**, alors
   que ses derniers témoins disparaissent et que les discours négationnistes
   et complotistes se banalisent
7. **Former l’administration, en particulier la police, la justice, et
   les services de renseignement, à la lutte contre l'antisémitisme et
   ses formes les plus violentes**, et lui donner les moyens nécessaires
   à la protection de la vie des juives et juifs de France ou présent-e-s
   sur le territoire français
8. Inscrire pleinement la France dans la `Stratégie européenne de lutte
   contre l'antisémitisme <https://ec.europa.eu/commission/presscorner/detail/fr/ip_21_4990>`_
   et pour la protection de la vie juive, élaborée par la Commission Européenne
9. **Condamner fermement les propos antisémites exprimés sous prétexte
   d’antisionisme, particulièrement quand ils appellent à la destruction
   de l’État d’Israël, à l’expulsion de ses habitants juifs ou font appel
   à des stéréotypes antisémites**; sans renoncer à la critique légitime
   des politiques gouvernementales israéliennes, qui ne saurait être
   assimilée à de l'antisémitisme
10. **Participer, une fois élu-e, à au moins une formation sur le sujet
    de l'antisémitisme**
