
.. _gt_antisemitisme:

==========================================================================
Création du groupe de travail : **Lutte contre l’antisémitisme** EELV ️✌️
==========================================================================

- https://twitter.com/Antisemitisme22
- https://www.charte-lutte-antisemitisme.fr/les-signataires
- https://www.eelv.fr/files/2021/10/MB-GT-antisemitisme-CF2021100203.pdf
- :ref:`motion_gt_antisemitisme`

.. @nadineherrati @greguti @FromontNathali4 @NathanGuedj_
.. Cyrielle Chatelain ✌️

Le GT Lutte contre l'antisémitisme
=======================================

:ref:`Créé en octobre 2021 <motion_gt_antisemitisme>` par un vote à
l'unanimité du Conseil Fédéral du parti Europe Ecologie - Les Verts, le
groupe de travail **Lutte contre l'antisémitisme** s'est notamment donné
pour mission "d'interroger les programmes de l’écologie politique aux
échéances électorales (y compris les élections présidentielles et législatives
de 2022) du point de vue de la lutte contre l’antisémitisme"
(lire `l'intégralité de la motion sur le site du parti <https://www.eelv.fr/mb-gt-ri-antisemitisme-cf2021100203/>`_).

Le groupe de travail est animé par:

- Nadine Herrati |Nadine Herrati| (https://twitter.com/nadineherrati), élue à Gentilly
  et co-secrétaire EELV pour le Val-de-Marne,
- Grégory Gutierez |Gregory Gutierez| (https://pouet.chapril.org/@Greguti/, @Greguti@pouet.chapril.org), élu à Malakoff
  dans les Hauts-de-Seine,
- Nathalie Fromont |Nathalie Fromont|  (https://twitter.com/FromontNathali4), militante EELV à Toulouse
- Nathan Guedj |Nathan Guedj| (https://twitter.com/NathanGuedj\_), co-secrétaire EELV Rhône.

Compte twitter:

- https://twitter.com/Antisemitisme22

