#
# Configuration file for the Sphinx documentation builder.
# http://www.sphinx-doc.org/en/stable/config

import platform
from datetime import datetime
from zoneinfo import ZoneInfo

import sphinx
import sphinx_material

project = "Combattre l'antisémitisme"
html_title = project
html_logo = "_static/logo.png"
author = "humain·e"
now = datetime.now(tz=ZoneInfo("Europe/Paris"))
version = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H ({now.tzinfo})"

today=version

extensions = [
    "sphinx.ext.intersphinx",
    "myst_parser",
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]
html_static_path = ["_static"]
html_show_sourcelink = True
html_sidebars = {
    "**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]
}
extensions.append("sphinx_material")
html_theme_path = sphinx_material.html_theme_path()
html_context = sphinx_material.get_html_context()
html_theme = "sphinx_material"

extensions += ["sphinx.ext.todo"]
todo_include_todos = True
html_theme_options = {
    "base_url": "https://france1.frama.io/combattrelantisemitisme/",
    "repo_url": "https://framagit.org/france1/combattrelantisemitisme",
    "repo_name": project,
    "html_minify": False,
    "html_prettify": True,
    "css_minify": True,
    "repo_type": "gitlab",
    "globaltoc_depth": -1,
    "color_primary": "green",
    "color_accent": "cyan",
    "theme_color": "#2196f3",
    "nav_title": f"{project} ({today})",
    "master_doc": False,
    "nav_links": [
        {
            "href": "genindex",
            "internal": True,
            "title": "Index",
        },
        {
            "href": "https://www.charte-lutte-antisemitisme.fr/",
            "internal": False,
            "title": "Charte NUPES",
        },
        {
            "href": "http://golema.net/",
            "internal": False,
            "title": "Golema",
        },
        {
            "href": "https://k-larevue.com/",
            "internal": False,
            "title": "K. la revue",
        },
        {
            "href": "https://raar.info/",
            "internal": False,
            "title": "RAAR",
        },
    ],
    "heroes": {
        "index": "Combattre l'antisémitisme",
    },
    "table_classes": ["plain"],
}


language = "en"
html_last_updated_fmt = ""

todo_include_todos = True

html_use_index = True
html_domain_indices = True

copyright = f"2020-{now.year}, {author} Built with sphinx {sphinx.__version__} Python {platform.python_version()}"

rst_prolog = """
.. |Nadine Herrati| image:: /images/nadine_herrati.jpg
.. |Nathalie Fromont| image:: /images/nathalie_fromont.png
.. |Gregory Gutierez| image:: /images/gregory_gutierez.jpg
.. |Nathan Guedj| image:: /images/nathan_guedj.jpg
"""
