

.. index::
   pair: Définitions ; Sionisme


.. index::
   pair: Définitions ; Sionisme

.. _definitions_sionisme:

=========================
Définitions du sionisme
=========================

.. contents::
   :depth: 3


Définition 1
=============


Salut,

Littéralement le sionisme c'est la théorie de création d'un foyer puis d'un
État juif.
(Nous sommes, anarchistes, contre la création d'un État mais de tous les États,
pas seulement de celui là...)

Or cet État existe depuis 1948. On ne peut plus, depuis le temps, être contre
sa création, **il est**.
Même s'il s'est créé dans les pires conditions d'un État colonial, même s'il
mène une politique d'apartheid, même s'il est théocratique, il est.

Littéralement **l'antisionisme signifie la destruction de l'État d'Israël**.

Je suis pour sa transformation en une terre laïque, multiconfessionnelle,
multiculturelle, plurielle, en paix avec ses voisins, mais **pas pour sa
destruction. Donc je ne suis pas antisioniste**.

Et tant qu'on ne trouvera pas un autre mot pour signifier l'opposition à la
politique interne et externe de cet État, **je ne me définirais pas comme
antisioniste**.

Même si cette criminalisation du terme antisionisme est une aubaine pour
ceux qui défendent sa politique.
