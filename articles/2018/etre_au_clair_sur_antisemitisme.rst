
.. index:
   pair: Antisémitisme ; Etre clair sur l'antisémitisme


.. _etre_clair_sur_antisemitisme:

=======================================
**Être au clair sur l’antisémitisme**
=======================================

Source:

- https://leseumcollectif.wordpress.com/2018/05/05/etre-au-clair-sur-lantisemitisme/
- https://twitter.com/JJR_JJR_JJR/status/993164707732303874


.. contents::
   :depth: 3


Introduction
=============

La lutte contre l’antisémitisme bute sur un paradoxe : l’antisémitisme
est omniprésent mais les antisémites revendiqués sont marginaux.
On observe un phénomène analogue à propos d’autres oppressions
systémiques quand de fortes mobilisations ont fini par marquer du
sceau de l’infamie des idéologies criminelles. La résolution de ce
paradoxe est pourtant immédiate : quand quelqu’un assure *n’être
pas antisémite* il n’y a tout simplement aucun crédit à accorder
a priori à cette déclaration.

Ce texte s’adresse à tou-te-s celleux qui se réclament d’une
manière ou d’une autre de la lutte contre le racisme. Plutôt que de
recenser en vain les qualités qui garantiraient un-e bon-ne allié-e
*certifié-e pas antisémite*, on a listé ici sans exhaustivité des
tendances (parfois très) problématiques observées dans nos milieux et
qui sont incompatibles avec un engagement clair contre l’antisémitisme.


Dans l’antiracisme autonome ou la gauche radicale
===================================================

1. Si tu n’abordes jamais la question de l’antisémitisme sauf pour
   dénoncer *le chantage à l’antisémitisme*, tu n’es pas au clair
   sur l’antisémitisme.

2. Si dans ton esprit, la catégorie des *racisé-e-s* n’inclut
   pas les Jui-f-ve-s ou si tu prétends que *les Jui-f-ve-s ne sont pas
   racisé-e-s*, tu n’es pas au clair sur l’antisémitisme.

3. Si tu instrumentalises la lutte contre l’antisémitisme en suggérant
   que tel ou tel acte ou propos raciste aurait davantage fait réagir s’il
   avait visé des Jui-f-ve-s, tu n’es pas au clair sur l’antisémitisme.

4. Si tu dénonces un soi-disant *philosémitisme d’Etat*, tu n’es
   pas au clair sur l’antisémitisme.

5. Si tu considères que le simple fait qu’une accusation
   d’antisémitisme soit portée par des réactionnaires ou des racistes
   suffit à la disqualifier, tu n’es pas au clair sur l’antisémitisme.

6. Si tu ne peux pas traiter de la question de l’antisémitisme sans
   t’empresser de changer de sujet en abordant d’autres formes de racisme,
   tu n’es pas au clair sur l’antisémitisme.

7. Si tu prétends que les Jui-f-ve-s contrôlent la finance, la politique,
   les médias ou carrément le monde ou que tu diffuses des théories du
   complot de toutes sortes, tu n’es pas au clair sur l’antisémitisme.

8. Si tu ergotes sur la pertinence du terme d’*antisémitisme*
   au motif que *le Judaïsme est une religion, pas une race* ou que
   *tous les sémites ne sont pas Jui-f-ve-s* , tu n’es pas au clair
   sur l’antisémitisme.

9. Si tu prétends que les Jui-f-ve-s sont la cible de violences mais
   pas de discriminations, tu n’es pas au clair sur l’antisémitisme.

10. Si tu minimises l’antisémitisme à gauche en y voyant la simple
    expression d’un *anticapitalisme des imbéciles*, tu n’es pas au
    clair sur l’antisémitisme.

11. Si non-Jui-f-ve, plutôt que de te remettre en question face à
    des accusations d’antisémitisme émanant de camarades Jui-f-ve-s,
    tu as le réflexe de rejeter ces accusations, tu n’es pas au clair
    sur l’antisémitisme.

12. Si tu décris la circoncision comme une pratique barbare, tu n’es
    pas au clair sur l’antisémitisme.

13. Si tu milites contre l’abattage rituel, y compris sous couvert de
    véganisme militant, tu n’es pas au clair sur l’antisémitisme.

14. Si tu te plains qu’*on parle trop de la Shoah* ou que tu
    dénonces la *pornographie mémorielle*, tu n’es pas au clair sur
    l’antisémitisme.

15. Si tu dénonces *le communautarisme*, tu n’es pas au clair
    sur l’antisémitisme.

16. Si tu prétends que la France de Vichy *a sauvé des Jui-f-ve-s*
    ou que tu fais la promotion de thèses négationnistes, tu n’es pas au
    clair sur l’antisémitisme.

17. Si ta lutte antiraciste prend en compte tous les avatars du racisme
    à l’exception de l’antisémitisme, tu n’es pas au clair sur
    l’antisémitisme.

18. Si tu emploies le name-dropping de personnalités juives honnies
    comme levier de mobilisation, tu n’es pas au clair sur l’antisémitisme.

19. Si tu prétends que les Jui-f-ve-s n’ont pas été soumis partout où
    iels ont vécu-e-s, à un statut d’infériorité assorti de persécutions,
    tu n’es pas au clair sur l’antisémitisme.

20. Si au nom d’une conception dévoyée de la laïcité, tu t’opposes
    au port de signes religieux ostentatoires à l’Ecole ou même dans
    l’espace public, que tu dénonces les menus de substitution dans les
    cantines scolaires ou les absences liées aux fêtes religieuses, tu
    n’es au clair ni sur l’antisémitisme ni sur l’islamophobie.

21. Si tu as manifesté pour la *séparation du CRIF et de l’Etat*,
    tu n’es pas au clair sur l’antisémitisme.

22. Si tu suspectes de double allégeance des politicien-ne-s français-e-s
    au motif qu’iels sont Jui-f-ves ou marié-es à un-e Jui-f-ve, tu n’es
    pas au clair sur l’antisémitisme.

23. Si tu déplores la loi réprimant la contestation de l’existence des
    crimes contre l’humanité, tu n’es pas au clair sur l’antisémitisme.

24. Si tu appelles *censure* la mobilisation ayant permis la «
    suspension* de la réédition des pamphlets antisémites de Céline
    par Gallimard (qui souligne ne pas y renoncer), tu n’es pas au clair
    sur l’antisémitisme.

25. Si tu présentes Israël comme le principal obstacle à la paix dans
    le monde, tu n’es pas au clair sur l’antisémitisme.

Au quotidien
==============

26. Si tu emploies des expressions comme *en Juif* pour dire *tout
    seul* ou *fais pas ton Juif* pour reprocher à quelqu’un son
    avarice, tu n’es pas au clair sur l’antisémitisme.

27. Si non-Jui-f-ve-s, tu pratiques l’humour oppressif à l’encontre
    des Jui-f-ve-s, peu importe que tu invoques Desproges à tort et à travers,
    tu n’es pas au clair sur l’antisémitisme.

28. Si un-e bourgeois-e Jui-f-ve est plus odieus-e à tes yeux qu’un-e
    bourgeois-e blanc-he, tu n’es pas au clair sur l’antisémitisme.

29. Si un-e prolétaire Jui-f-ve est un peu suspect-e à tes yeux, tu
    n’es pas au clair sur l’antisémitisme.

30. Si en voyant un enfant portant une kippa, tu sens monter en toi
    *le prélude de [ta] ruine intérieure*, tu n’es pas au clair sur
    l’antisémitisme.

31. Si tu crois qu’il existe un *privilège juif*, tu n’es pas
    au clair sur l’antisémitisme.

32. Si tu reproches aux Jui-f-ve-s de pratiquer l’entraide, tu n’es
    pas au clair sur l’antisémitisme.

33. Si tu t’entêtes à ne voir qu’un crime crapuleux dans
    l’assassinat d’Ilan Halimi ou de Mireille Knoll, tu n’es pas au clair
    sur l’antisémitisme.

En prétendant lutter contre l’antisémitisme
=============================================

34. Si tu as signé le `manifeste contre le nouvel antisémitisme`_
    ou que tu détournes la lutte contre l’antisémitisme au profit d’un
    combat islamophobe, dédouanant au passage le suprémacisme blanc de ses
    responsabilités, tu n’es pas au clair sur l’antisémitisme.

35. Si tu instrumentalises la Shoah ou l’antisémitisme pour justifier
    la persécution des Palestinien-ne-s ou disqualifier leurs revendications
    légitimes, tu n’es pas au clair sur l’antisémitisme.

36. Si tu ériges les Jui-f-ve-s en minorité modèle, tu n’es pas au
    clair sur l’antisémitisme.


.. _`manifeste contre le nouvel antisémitisme`:  https://juivesetjuifsrevolutionnaires.wordpress.com/2018/04/25/manifeste-des-300-la-lutte-contre-lantisemitisme-nest-pas-soluble-dans-lislamophobie/

En soutien aux luttes Palestiniennes
=====================================


37. Si tu dénonces plus volontiers le *sionisme* que la colonisation
    ou l’occupation, tu n’es pas au clair sur l’antisémitisme.

38. Si tu crois normal d’exiger des Juif-ve-s de France qu’iels
    se *désolidarisent* d’Israël, tu n’es pas au clair sur
    l’antisémitisme.

39. Si des débordements antisémites ne sauraient ternir à tes yeux
    le succès d’une large mobilisation en soutien au peuple Palestinien,
    tu n’es pas au clair sur l’antisémitisme.

40. Si confronté à l’antisémitisme dans ton mouvement, tu te réfugies
    derrière une caution juive, tu n’es pas au clair sur l’antisémitisme.

41. Si tu emploies le terme *sioniste* comme une insulte, tu n’es
    pas au clair sur l’antisémitisme.

42. Si tu qualifies de *génocide* les massacres et le nettoyage
    ethnique perpétrés par Israel à l’encontre des Palestinien-ne-s
    pour suggérer que les victimes d’un génocide seraient devenues les
    perpétrateurs d’un autre, tu n’es pas au clair sur l’antisémitisme.

43. Si tu promeus une *solution* au conflit Israélo-Palestinien
    exigeant l’exil massif des Israélien-ne-s Jui-f-ve-s, tu n’es pas
    au clair sur l’antisémitisme.

44. Si non-Jui-f-ve, tu invoques des préceptes de théologie juive pour
    défendre ton *antisionisme* ou ton *sionisme*, tu n’es pas au
    clair sur l’antisémitisme.

45. Enfin, si tu n’es pas au clair sur l’antisémitisme et que tu
    instrumentalises les souffrances des Palestinen-ne-s au service de ta
    haine des Jui-f-ve-s, tu n’es pas non plus au clair avec les luttes
    Palestiniennes.


