

.. _corbyn_antisemitisme_2021_10_27:

======================================================================================================================
Mais que se passait-il dans le Labour de Corbyn ? par Milo Lévy-Bruhl & Adrien Zirah le mercredi 27 octobre 2021
======================================================================================================================

- https://k-larevue.com/mais-que-se-passait-il-dans-le-labour-de-corbyn/
- https://www.equalityhumanrights.com/en/publication-download/investigation-antisemitism-labour-party
- https://twitter.com/MiloMLB
- https://www.equalityhumanrights.com/en, https://twitter.com/EHRC
- https://labouragainstas.org.uk/about, https://twitter.com/LabourAgainstAS
- https://www.jewishlabour.uk/, https://twitter.com/JewishLabour
- https://antisemitism.org, https://twitter.com/antisemitism


Retour sur le rapport de l’EHRC sur l’antisémitisme au sein du Labour
==========================================================================

Il y a tout juste un an — le 29 octobre 2020 — Jeremy Corbyn était exclu
du Labour qu’il avait dirigé de 2015 à 2020.

Cette exclusion faisait suite aux réserves qu’il avait exprimées sur les
conclusions du rapport de l’:term:`EHRC` portant sur l’antisémitisme au sein du
Labour remis à son successeur Keir Starmer.

La question de l’antisémitisme d’une partie de la gauche n’est pas
spécifiquement anglaise. Elle est régulièrement posée en France également.

Pourtant, ce rapport a peu été repris par les médias français. K. en
présente ici une synthèse. Elle rend compte à la fois de ce que fut la
réalité de l’antisémitisme au sein du Labour mais aussi de la manière dont,
après la démission de Corbyn, les travaillistes surent le regarder en face.

Étrangement, alors qu’une partie de la gauche française s’immisçait volontiers
dans les polémiques anglaises pour soutenir Corbyn, elle n’a pas trouvé
bon de revenir sur ce rapport.


L’enquête sur l’antisémitisme au sein du Labour d’un organisme indépendant, l’EHRC
======================================================================================

Introduction
---------------

- https://www.lemonde.fr/international/article/2019/12/11/la-communaute-juive-se-detourne-du-labour-de-jeremy-corbyn_6022528_3210.html
- https://www.lemonde.fr/international/article/2018/04/03/parti-travailliste-britannique-jeremy-corbyn-a-la-peine-face-aux-accusations-d-antisemitisme_5279974_3210.html
- https://www.lemonde.fr/international/article/2019/10/17/antisemitisme-nouvelle-demission-au-sein-du-labour_6015799_3210.html
- https://www.thetimes.co.uk/article/corbyn-supported-antisemitic-mural-mc7c9qxw6


**La maison des Juifs**, c’est aussi comme ça que le Labour fut longtemps
surnommé, tant le vote juif britannique inclinait majoritairement vers
le travaillisme.

Mais de ce point de vue-là, le mandat de Corbyn marque une rupture inédite
entre le Labour et les juifs britanniques.

Rapidement, le parti dut en effet faire face à une `vague de démissions <https://www.lemonde.fr/international/article/2019/10/17/antisemitisme-nouvelle-demission-au-sein-du-labour_6015799_3210.html>`_
ainsi qu’à de nombreuses critiques émanant des représentants de la
communauté juive au `sein[3] <https://www.lemonde.fr/international/article/2019/12/11/la-communaute-juive-se-detourne-du-labour-de-jeremy-corbyn_6022528_3210.html>`_ et en-dehors du Labour.

Elles entendaient dénoncer un parti gangréné par l’antisémitisme que son
leader laisserait prospérer. Une vive polémique éclata notamment en mars 2018,
lorsque resurgit une `prise de position de Jeremy Corbyn sur Facebook <https://www.thetimes.co.uk/article/corbyn-supported-antisemitic-mural-mc7c9qxw6>`_,
datée de 2012, dans laquelle il s’opposait au retrait d’une fresque murale
(voir image d’illustration de l’article) dans l’est londonien, représentant
des banquiers juifs, et reprenant clairement des tropes antisémites.

Prise de position sur laquelle il revint `alors[4] <https://www.lemonde.fr/international/article/2018/04/03/parti-travailliste-britannique-jeremy-corbyn-a-la-peine-face-aux-accusations-d-antisemitisme_5279974_3210.html>`_, expliquant ne pas avoir
examiné « suffisamment attentivement » cette fresque « profondément antisémite ».

Ni les démissions, ni les polémiques, n’empêchèrent Jeremy Corbyn de se
maintenir à la tête du Labour. Ce n’est qu’à la suite de la défaite
historique du Labour de décembre 2019, imputable à ses errements sur
la question du Brexit, que Corbyn démissionna avant d’être remplacé
par Keir Starmer.

Publication du rapport par l'EHRC (Equality and Human Rights Commission)
-----------------------------------------------------------------------------

- https://www.equalityhumanrights.com/en/publication-download/investigation-antisemitism-labour-party
- https://www.equalityhumanrights.com/sites/default/files/investigation-into-antisemitism-in-the-labour-party.pdf
- https://www.equalityhumanrights.com/en/about-us/who-we-are
- https://www.equalityhumanrights.com/en/our-legal-action/our-powers

En octobre 2020, quelques mois après le changement de direction à la tête
du Labour, les conclusions de « l’Enquête sur l’antisémitisme au sein du
Parti travailliste » (Investigation into antisemitism in the Labour Party)
furent rendues par `l’EHRC[5] <https://www.equalityhumanrights.com/en/publication-download/investigation-antisemitism-labour-party>`_.

La Commission pour l’Égalité et les Droits de l’Homme (Equality and Human
Rights Commission, désormais :term:`EHRC`) est un organisme britannique public
et indépendant, accrédité par les Nations Unies, créé en 2007 à la suite
de l’Equality Act 2006 (complété par l’Equality Act 2010).

Son rôle, `tel que décrit sur son site <https://www.equalityhumanrights.com/en/about-us/who-we-are>`_, est de **lutter contre la discrimination,
promouvoir l’égalité des chances et protéger les droits de l’homme**, et
il dispose à cette fin de différentes prérogatives légales, qui – comme
`il le précise <https://www.equalityhumanrights.com/en/our-legal-action/our-powers>`_ – vont de l’assistance et du conseil à la réalisation
d’enquêtes et d’actions en justice.

Début de l'enquête en mai 2019
---------------------------------

- https://www.equalityhumanrights.com/sites/default/files/investigation-into-antisemitism-in-the-labour-party.pdf

L’enquête menée par l’:term:`EHRC` a été initiée dès le mois de mai 2019 « à la
suite de graves inquiétudes publiques concernant des allégations
d’antisémitisme et d’un certain nombre de plaintes officielles adressées
[à l’:term:`EHRC`][6] » (`p. 5 du rapport <https://www.equalityhumanrights.com/sites/default/files/investigation-into-antisemitism-in-the-labour-party.pdf>`_),
émanant notamment de Campaign Against Antisemitism (ONG britannique fondée en 2014)
et du Jewish Labour Movement[7]
(The Jewish Labour Movement, précédemment connu sous le nom de Poale Zion (Britain)
de 1903 à 2004, est l’une des plus anciennes organisations socialistes
(« socialist societies ») affiliées au parti travailliste britannique)

L’enquête visait à déterminer si le Labour avait enfreint l’Equality Act 2010,
dans son attitude envers le judaïsme et les juifs, par l’intermédiaire
de ses employés ou agents.

Elle cherchait également à mesurer la manière dont le Labour avait mis
en œuvre les recommandations de précédents rapports – notamment le rapport
de la spécialiste des droits civiques Shami Chakrabarti de 2016,
commandé par Jeremy Corbyn lui-même à la suite de premières polémiques
sur l’antisémitisme au sein du parti -, et si le Labour avait mis en place
une procédure de traitement des plaintes pour antisémitisme en son sein
qui soit légitime, cohérente et efficace.

L’enquête s’est basée sur l’examen de 70 dossiers d’enquête relatifs à
des plaintes pour antisémitisme faites au sein du parti (sur plus de 220
plaintes identifiées), 58 ayant été sélectionnés par l’:term:`EHRC`, et 12 proposés
par le Labour.

L’:term:`EHRC` a également examiné des témoignages au sein du parti travailliste
et de différentes organisations, dont le Campaign Against Antisemitism,
le Jewish Labour Movement et The Jewish Voice for Labour[8].


Un rapport accablant mettant en évidence une “culture” problématique
-----------------------------------------------------------------------

- https://www.equalityhumanrights.com/sites/default/files/investigation-into-antisemitism-in-the-labour-party.pdf

Dans son développement comme dans ses conclusions, le rapport est accablant
sur la gestion qui a été faite des cas d’antisémitisme sous l’ère Corbyn,
et plus particulièrement du **traitement des plaintes pour antisémitisme**.

Plus largement, il conclut que le parti est responsable
**d’actes illégaux de harcèlement et de discrimination** (`p. 6 <https://www.equalityhumanrights.com/sites/default/files/investigation-into-antisemitism-in-the-labour-party.pdf>`_).

Outre les cas éminemment problématiques que nous allons aborder plus en
détail, il semble qu’une forme de désinvolture ait marqué la stratégie
de lutte contre l’antisémitisme, à rebours de l’engagement de Jeremy Corbyn
sur une politique de tolérance zéro vis-à-vis de l’antisémitisme.

Le rapport de l’:term:`EHRC` note par exemple dès l’abord que malgré l’engagement
du Labour à collaborer avec eux, de nombreuses difficultés ont été
constatées dans la transmission des documents, si bien que l’:term:`EHRC`
s’interroge sur l’engagement réel du Labour à coopérer.

Surtout et plus largement, le rapport « met en évidence une culture à
l’intérieur du Parti qui, au mieux, n’a pas fait assez pour prévenir
l’antisémitisme et, **au pire, pourrait être considérée comme l’acceptant** (`p. 101 <https://www.equalityhumanrights.com/sites/default/files/investigation-into-antisemitism-in-the-labour-party.pdf>`_).


Plusieurs cas permettent selon le rapport de tenir le Labour pour
responsable de **harcèlement envers les juifs**.

En effet, en vertu de l’Equality Act 2010, un parti politique peut être
tenu pour légalement responsable des comportements illicites de ses agents
dès lors que ces agissements sont commis dans le cadre de leurs fonctions
au sein du parti, en tant qu’élu (membre du parlement, maire, etc.) ou
en tant que membre des différentes instances du `parti[9]
(Les différents cas pour lesquels le Labour peut être tenu pour légalement
responsable des agissements de ses agents sont détaillés dans l’Annexe 3 du rapport, p. 112-116) <https://www.equalityhumanrights.com/sites/default/files/investigation-into-antisemitism-in-the-labour-party.pdf>`_

Près d’une vingtaine de cas d’agents officiels du parti, parmi ceux étudiés
par l’:term:`EHRC` (« la partie émergée de l’iceberg », selon `l’expression du rapport, p. 8 <https://www.equalityhumanrights.com/sites/default/files/investigation-into-antisemitism-in-the-labour-party.pdf>`_),
sont considérés comme **borderline**.

De nombreux autres sont considérés **comme des cas d’attitudes clairement
antisémites** mais issues de membres *ordinaires* du parti, et dont le
Labour ne peut donc pas être tenu pour responsable.

Deux principaux cas sont en revanche détaillés dans le rapport, ceux de
Ken Livingstone et Pam Bromley[10].

Le Labour responsable de harcèlement[11] : les cas de Ken Livingstone et Pam Bromley
----------------------------------------------------------------------------------------

- https://www.lefigaro.fr/flash-actu/2016/04/27/97001-20160427FILWWW00307-gb-une-deputee-travailliste-suspendue-apres-des-propos-juges-antisemites.php?web=1&wdLOR=c590C6A44-56B2-7F41-B1E6-C16D9EC98099
- https://fr.timesofisrael.com/les-juifs-britanniques-accueillent-le-retour-de-la-deputee-repentie-naz-shah/

Les messages antisémites de Naz Shah
+++++++++++++++++++++++++++++++++++++++++

Le Labour responsable de harcèlement[11] : les cas de Ken Livingstone et Pam Bromley

Député de la Chambre des communes de 1987 à 2001, maire de Londres de
2000 à 2008 et très proche de Jeremy Corbyn, Ken Livingstone était
membre de l’organe directeur du Labour, le National Executive Committee (NEC),
au moment des faits.

En 2014, la future députée travailliste Naz Shah avait partagé sur Facebook
un post proposant de **transporter** la population israélienne aux `États-Unis[12] <https://www.lefigaro.fr/flash-actu/2016/04/27/97001-20160427FILWWW00307-gb-une-deputee-travailliste-suspendue-apres-des-propos-juges-antisemites.php?web=1&wdLOR=c590C6A44-56B2-7F41-B1E6-C16D9EC98099>`_ .

Le post comportait une image montrant l’État d’Israël incrusté dans celui
des États-Unis avec le commentaire : « problème résolu » (problem solved).

Un autre de ses posts Facebook comparait l’État d’Israël à l’Allemagne nazie,
expliquant, avec le hashtag #IsraelApartheid : « Nous ne devrions jamais
oublier que tout ce que Adolf Hitler a fait en Allemagne était légal »
(« We should never forget that everything that Hitler did in Germany was legal »).

Ces posts de Naz Shah, élue députée du Labour en 2015, ressurgissent en
avril 2016 et créent une vive polémique. La députée s’excuse alors
`publiquement[13] <https://www.lefigaro.fr/flash-actu/2016/04/27/97001-20160427FILWWW00307-gb-une-deputee-travailliste-suspendue-apres-des-propos-juges-antisemites.php?web=1&wdLOR=c590C6A44-56B2-7F41-B1E6-C16D9EC98099>`_
et devant le Parlement pour ses propos.

Elle est rapidement suspendue par le Labour le temps d’une enquête `administrative [14]
(Elle sera réintégrée en juillet 2016 après avoir réitéré ses excuses,
notamment lors d’une visite à la synagogue de Leeds) <https://fr.timesofisrael.com/les-juifs-britanniques-accueillent-le-retour-de-la-deputee-repentie-naz-shah/>`_

.. _affaire_ken_livingstone:

Démission de Ken Livingstone suite à son soutien à Naz Shah
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://www.equalityhumanrights.com/sites/default/files/investigation-into-antisemitism-in-the-labour-party.pdf
- [15] https://www.lemonde.fr/europe/article/2017/04/07/au-royaume-uni-le-labour-se-dechire-sur-les-propos-antisemites-de-ken-livingstone_5107496_3214.html
- [16] https://www.lemonde.fr/international/article/2018/05/21/accuse-d-antisemitisme-l-ancien-maire-de-londres-ken-livingstone-demissionne-du-parti-travailliste_5302474_3210.html

Suite à ce retrait, Ken Livingstone intervient à plusieurs reprises dans
les médias pour nier le caractère antisémite des posts de Naz Shah,
expliquant que les critiques à son égard font partie d’une campagne de
diffamation menée par le « lobby israélien » pour stigmatiser toute critique
d’Israël comme étant antisémite, et miner le mandat de Jeremy Corbyn.

Il ajoute que Hitler avait soutenu le sionisme en 1932, « avant de devenir
fou et de finir par tuer six millions de juifs »`[15 voir Le Monde, 07/04/2017] <https://www.lemonde.fr/europe/article/2017/04/07/au-royaume-uni-le-labour-se-dechire-sur-les-propos-antisemites-de-ken-livingstone_5107496_3214.html>`_

**Ces propos suscitent de vives réactions, plus d’une centaine de députés
travaillistes demandant son exclusion du parti**.

Mis en retrait suite à ces propos le temps de l’enquête, il sera finalement
sanctionné d’une `suspension de deux ans du parti[16, Ken Livingstone finit par démissionner du Labour en mai 2018] <https://www.lemonde.fr/international/article/2018/05/21/accuse-d-antisemitisme-l-ancien-maire-de-londres-ken-livingstone-demissionne-du-parti-travailliste_5302474_3210.html>`_.


L’:term:`EHRC` conclut au caractère antisémite des propos de Ken Livingstone,
ayant eu pour effet de **créer un environnement intimidant, hostile,
dégradant, humiliant ou offensant pour les membres et les futurs membres
du parti travailliste, en particulier ceux qui sont juifs** `(p. 107) <https://www.equalityhumanrights.com/sites/default/files/investigation-into-antisemitism-in-the-labour-party.pdf>`_

Elle conclut également à la responsabilité du Labour dans la conduite
de Ken Livingstone, dans la mesure où il occupait des fonctions
officielles au sein du Labour.


Expulsion de Pam Bromley pour antisémitisme en février 2020
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://www.equalityhumanrights.com/sites/default/files/investigation-into-antisemitism-in-the-labour-party.pdf

Conseillère municipale du parti travailliste à Rossendale (Lancashire),
Pam Bromley a, quant à elle, posté un certain nombre de messages sur
Facebook pour lesquels le Labour a reçu de nombreuses plaintes l’accusant
d’antisémitisme.

Ces posts remontant au moins à 2017 déploient certains préjugés antisémites
classiques, comme l’idée que le peuple juif contrôle le monde financier.

Pam Bromley y explique également que les plaintes pour antisémitisme au
sein du Labour sont de pures fabrications, et suggère que les juifs ont
fomenté un complot pour prendre le contrôle du parti travailliste, faisant
référence notamment à une `« cinquième colonne[17] (On peut trouver les posts Facebook en question aux pages 108-109 du rapport) <https://www.equalityhumanrights.com/sites/default/files/investigation-into-antisemitism-in-the-labour-party.pdf>`_

Suspendue en avril 2018, Pam Bromley a été expulsée du parti en février 2020.

L’:term:`EHRC` estime cependant que le Labour peut être tenu pour responsable de
son attitude, dans la mesure où elle exerçait des fonctions officielles,
mentionnées explicitement sur son compte Facebook, et a utilisé pour
certains de ces posts son compte Facebook officiel de conseillère municipale.

Un traitement erratique et politisé des plaintes pour antisémitisme
-----------------------------------------------------------------------

- https://www.equalityhumanrights.com/sites/default/files/investigation-into-antisemitism-in-the-labour-party.pdf


Le rapport revient longuement sur les **lacunes du processus de gestion
interne des plaintes pour les cas d’antisémitisme**.

Les plaintes déposées par les membres du parti doivent en effet permettre
d’enquêter sur une potentielle rupture du code de conduite.

Depuis 2018, ces plaintes sont enregistrées sur Internet, et examinées
d’abord par une instance du Labour, le Governance and Legal Unit (GLU),
qui décide s’il faut ou non réaliser une enquête.
Si c’est le cas, le GLU réalise l’enquête et en transmet les résultats
au National Executive Committee (NEC), qui juge s’il y a ou non infraction,
sa gravité et la sanction appropriée.

Si nécessaire, une audition peut être organisée par le National Constitutional
Committee (NCC).

Or, dans les cas examinés, l’:term:`EHRC` conclut à de **graves carences dans
le traitement des plaintes pour antisémitisme** : elle juge les réponses
données par le parti à ces plaintes comme globalement **incohérentes et opaques**.

Elle indique que ni les plaignants ni les accusés n’ont été tenus au
courant correctement du traitement et des résultats des plaintes, elle
signale des délais trop importants en vue de les instruire, elle remarque
une tenue de registre inexistante et **note une désinvolture criante** dans
le traitement de ces plaintes, lesquelles sont **parfois purement et
simplement oubliées**.

L’:term:`EHRC` mentionne notamment `(p. 70) <https://www.equalityhumanrights.com/sites/default/files/investigation-into-antisemitism-in-the-labour-party.pdf>`_
le cas d’un conseiller du Labour qui, en 2016, a partagé l’image
d’un banquier juif, Jacob Rothschild, sur sa page Facebook, avec une légende
disant que la famille Rothschild et d’autres institutions, dont la City
de Londres et le Vatican, « possèdent nos news, nos médias, notre pétrole,
et même nos gouvernements ».

La plainte à laquelle ce commentaire a donné lieu n’a jamais été traitée.

Aucune orientation générale ou procédure déterminée n’a donc été donnée
pour établir ce qui peut ou non être considéré comme de l’antisémitisme,
ni les sanctions à appliquer en fonction de la gravité des faits.

Aucune raison n’est donnée des décisions prises, pour les sanctions comme
pour les classements sans suite, et aucun compte-rendu n’est fait des
discussions qui donnent lieu à ces décisions.
De plus, les personnes en charge de traiter les plaintes pour antisémitisme
ne reçoivent aucune formation pratique spécifique pour mener à bien leur tâche.

L’:term:`EHRC` conclut ainsi que « les preuves montrent clairement que le système
de traitement des plaintes pour antisémitisme présentait de graves lacunes.
Il en résulte que les personnes déposant des plaintes pour antisémitisme
ont été mal servies par leur parti politique et que les personnes répondant
aux plaintes ont souvent été traitées injustement » `(p. 73) <https://www.equalityhumanrights.com/sites/default/files/investigation-into-antisemitism-in-the-labour-party.pdf>`_.


L’:term:`EHRC` pointe également un **traitement politisé des plaintes pour antisémitisme
via des ingérences politiques**, ce qui induit que le traitement des plaintes
ne suit pas les procédures officielles.

Cela a pu conduire à des décisions prises pour des raisons politiques et
non sur la base de l’examen des faits, conduisant potentiellement à un
effet discriminatoire.

**En effet, dans la procédure officielle, les équipes de Corbyn, en tant
que leader de l’opposition (Leader of the opposition, LOTO), ne sont pas
censées intervenir dans le traitement des plaintes.
Or, l’:term:`EHRC` a découvert des preuves de leur intervention dans différents
dossiers de plainte pour antisémitisme, à certaines périodes de manière
systématique**.

Affaire classée pour Corbyn
+++++++++++++++++++++++++++++++

**L’un d’eux concerne directement Jeremy Corbyn**. Une plainte avait été émise
en avril 2018 contre lui suite à l’affaire de la fresque murale.
Alors que la décision de procéder ou non à une enquête doit être prise
par le Governance and Legal Unit, un mail des équipes de Corbyn a demandé
à ce que la plainte soit classée sans suite, estimant que « la plainte
elle-même semble être bien en deçà du seuil requis pour une enquête » (`cité p. 44 <https://www.equalityhumanrights.com/sites/default/files/investigation-into-antisemitism-in-the-labour-party.pdf>`_).

Interventions de Corbyn
++++++++++++++++++++++++++++

- https://www.lemonde.fr/europe/article/2017/04/07/au-royaume-uni-le-labour-se-dechire-sur-les-propos-antisemites-de-ken-livingstone_5107496_3214.html

Dans :ref:`l’affaire Ken Livingstone <affaire_ken_livingstone>`, Jeremy Corbyn semble être intervenu directement,
cette fois pour **demander sa suspension**.

Faite sous les pressions médiatiques et politiques, et contre les procédures
officielles prévues par le Labour, la suspension de deux ans a été confirmée
ensuite par les instances disciplinaires du parti, provoquant la colère
de ceux qui au sein du Labour **souhaitaient son expulsion**.

En effet, à la différence du cas de Naz Shah, dont la réintégration a été
accueillie favorablement par les représentants juifs du Labour **du fait
de ses excuses répétées**, Ken Linvingstone `n’a jamais regretté ses propos[18, voir Le Monde, 29/04/2016], <https://www.lemonde.fr/europe/article/2017/04/07/au-royaume-uni-le-labour-se-dechire-sur-les-propos-antisemites-de-ken-livingstone_5107496_3214.html>`_
et traîne par ailleurs derrière lui **plusieurs polémiques liées à des propos
douteux sur les juifs**.

En 2005 notamment, alors maire de Londres, il avait comparé un journaliste
juif qui l’interrogeait à un gardien de camp de concentration, des propos
entraînant une vive polémique et sa suspension pour un mois de ses fonctions.


Des interventions non légitimes de Corbyn
+++++++++++++++++++++++++++++++++++++++++++++++

- https://www.equalityhumanrights.com/sites/default/files/investigation-into-antisemitism-in-the-labour-party.pdf

Quel que soit le sens de ces interventions, l’:term:`EHRC` estime qu’elles ne
sont pas légitimes, ajoutant de l’incohérence et de l’opacité aux
procédures disciplinaires du parti : **nous constatons que l’implication
du LOTO [Leader of the opposition] dans les plaintes individuelles pour
antisémitisme ne s’inscrivait pas dans le cadre de la procédure de traitement
des plaintes du parti travailliste, et ne constituait donc pas une
approche légitime pour déterminer les plaintes**.

Ce processus a entraîné un manque de transparence et de cohérence dans
la procédure de traitement des plaintes. Il a créé un **risque sérieux
de traitement discriminatoire** réel ou perçu dans des plaintes particulières.

Il a aussi fondamentalement miné la confiance du public dans la procédure
de traitement des plaintes ». `(p. 50) <https://www.equalityhumanrights.com/sites/default/files/investigation-into-antisemitism-in-the-labour-party.pdf>`_


Si l’:term:`EHRC` note que des progrès ont été faits dans le traitement des
plaintes pour antisémitisme à partir de 2019, ces multiples carences,
et particulièrement les faits d’ingérence politique, rendent selon
l’:term:`EHRC` le Labour responsable de discrimination `indirecte[19]
(Dans le cadre de l’Equality Act 2010, la discrimination indirecte signifie
le fait « d’appliquer d’une politique ou une pratique qui désavantage
particulièrement les personnes d’une certaine race ou d’une certaine
religion par rapport à une personne qui ne partage pas leur race ou leur
religion » (p. 21) <https://www.equalityhumanrights.com/sites/default/files/investigation-into-antisemitism-in-the-labour-party.pdf>`_


Légèreté sélective et électrochoc au sein du Labour
-------------------------------------------------------

- https://www.equalityhumanrights.com/sites/default/files/investigation-into-antisemitism-in-the-labour-party.pdf

Si de telles carences apparaissent particulièrement coupables aux yeux
de l’:term:`EHRC`, **c’est également parce qu’elles ne semblent concerner que
l’antisémitisme**.

L’:term:`EHRC` note en effet que **le traitement des plaintes pour antisémitisme
est différent du traitement des plaintes que l’on trouve sur d’autres
sujets**.

Le rapport prend ainsi à plusieurs reprises l’exemple du traitement des
plaintes pour harcèlement sexuel, pour lesquelles les procédures sont
cohérentes, transparentes et efficaces, et réalisées par un personnel
spécifiquement formé.

De plus, un certain nombre des carences liées au traitement des plaintes
avait déjà été signalé par le rapport Chakrabarty de 2016, dont les
recommandations n’ont finalement pas été suivies.

L’:term:`EHRC` ne peut faire autrement que de soupçonner, dans le meilleur des
cas, un désintérêt pour les questions liées à l’antisémitisme, mettant
directement en cause la direction du parti sous l’ère Corbyn :
**Le parti travailliste a montré sa capacité à agir de manière décisive
lorsqu’il le souhaite, en introduisant une procédure sur mesure pour
traiter les plaintes pour harcèlement sexuel**.

Il est difficile de ne pas conclure que l’antisémitisme au sein du parti
travailliste aurait pu être combattu plus efficacement si la direction
avait choisi de le faire. » `(p. 101) <https://www.equalityhumanrights.com/sites/default/files/investigation-into-antisemitism-in-the-labour-party.pdf>`_


Discours de Keir Starmer le 29 octobre 2020
-----------------------------------------------------------------------

- https://en.wikipedia.org/wiki/Keir_Starmer
- https://www.equalityhumanrights.com/sites/default/files/investigation-into-antisemitism-in-the-labour-party.pdf
- https://www.theguardian.com/news/2020/oct/29/keir-starmer-ehrc-report-day-of-shame-labour-antisemitism

Ce rapport, publié en octobre 2020, a fait l’effet d’une bombe au sein
du Labour.

**Keir Starmer, qui en succédant à Jeremy Corbyn à la tête du Labour a fait
de la lutte contre l’antisémitisme une de ses priorités**, a parlé d’un
`jour de honte pour le parti travailliste <https://www.theguardian.com/news/2020/oct/29/keir-starmer-ehrc-report-day-of-shame-labour-antisemitism>`_.

.. figure:: images/keir_starmer.jpg
   :align: center

   Keir Starmer : Le rapport sévère de l’EHRC sur l’antisémitisme est un « jour de honte » pour le parti travailliste. Discours du 29 /10/ 2020.

**Il a indiqué accepter sans aucune limite les conclusions du rapport et
s’est engagé à mettre en œuvre toutes les recommandations qui y sont
formulées via la mise en place d’un plan d’action**.

Peu après les déclarations de Keir Starmer, Jeremy Corbyn a cherché à
minimiser la portée du rapport, estimant que le nombre d’antisémites
au sein du Labour avait été exagéré.

**Keir Starmer a alors pris la décision de suspendre Jeremy Corbyn, le 29 octobre 2020**.
Réintégré quelques semaines plus tard, Jeremy Corbyn reste néanmoins,
pour l’instant, interdit de siéger dans l’opposition travailliste à la
chambre des Communes.


Les échos français des polémiques du Labour de Corbyn
=========================================================

- https://melenchon.fr/2019/12/13/corbyn-la-synthese-mene-au-desastre/

Parmi les grands partis européens issus du socialisme, il en est un qui
s’invita souvent dans les polémiques relatives à l’antisémitisme au
sein du Labour, c’est La France Insoumise.

Lors d’un voyage en Angleterre en septembre 2018, son leader,
Jean-Luc Mélenchon dénonçait ainsi « un tissu de reprises sans recul ni
preuve de la propagande de Netanyahou (…) » avant d’affirmer : « en général
quand une campagne électorale voit un homme de gauche être traité
d’antisémite c’est qu’il n’est pas loin du pouvoir. »

Et de conclure, « Bref le bon vieux rayon paralysant est en plein
fonctionnement ici. Écœurant. »

Manipulation israélienne, ou manipulation des dominants, la question de
savoir qui instrumentaliserait un antisémitisme inexistant pour nuire
à Corbyn n’était pas clair, mais le cœur de l’argument était posé : il
n’y a pas d’antisémitisme.

Le lendemain de la défaite de décembre 2019, `Jean-Luc Mélenchon reprenait
la plume <https://melenchon.fr/2019/12/13/corbyn-la-synthese-mene-au-desastre/>`_
pour dénoncer les erreurs du leader socialiste anglais et en  tirer
les leçons : « Corbyn a dû subir sans secours la grossière accusation
d’antisémitisme à travers le grand rabbin d’Angleterre et les divers
réseaux d’influence du Likoud. Au lieu de riposter, il a passé son temps
à s’excuser et à donner des gages. (…)
**Je n’y céderai jamais pour ma part**. Retraite à point, Europe allemande
et néolibérale, capitalisme vert, génuflexion devant les ukases arrogants
des communautaristes du CRIF : c’est non. Et non c’est non. ».

**La dernière phrase activant l’imagerie antisémite du groupe juif organisé
donnant des ordres à des politiques à genoux fera beaucoup parler**.

Mais le présupposé de base restait le même : **pour Mélenchon l’accusation
d’antisémitisme formulée à l’endroit de Corbyn était** *grossière*.

Quelques semaines plus tard, Mélenchon revenait une dernière fois sur
les accusations formulées à l’encontre de Corbyn dans un post de son
blog consacré « à l’antisémitisme comme prétexte » utilisé à son tour
contre Bernie Sanders : « Après la campagne harcelante contre Jeremy
Corbyn et les lourdes insinuations contre moi, c’est au tour de Bernie
Sanders d’être accusé d’être antisémite. »

Le rapport de l’:term:`EHRC` a levé le voile sur la réalité que cachait
cette **campagne harcelante**.
Pourtant, Jean-Luc Mélenchon n’a jamais cru bon d’y revenir.
Le « Jour de honte » du Labour n’a pas encore fait rougir outre-manche.

**Faut-il en conclure que le Labour ne sera pas un modèle cette fois-ci ?**

Milo Lévy-Bruhl & Adrien Zirah





