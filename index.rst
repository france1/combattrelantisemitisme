

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain">Framapiaf</a>
   <a rel="me" href="https://qoto.org/@pvergain">Qoto</a>
   <a rel="me" href="https://framapiaf.org/@goldman_bakounine">Framapiaf</a>
   <a rel="me" href="https://qoto.org/@goldman_bakounine">Qoto</a>


.. _combattre_antisemitisme:

=======================================
Combattre l'antisémitisme en France
=======================================

.. figure:: _static/logo.png
   :align: center
   :width: 200

.. toctree::
   :maxdepth: 7

   organisations/organisations

.. toctree::
   :maxdepth: 5

   definitions/definitions
   articles/articles
   glossaire/glossaire

